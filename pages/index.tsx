// import  from '@g-loot/react-tournament-brackets';
import React from 'react';
import dynamic from 'next/dynamic';
import { Navbar, Button } from 'flowbite-react';
import Players from '@/sections/Players';
import Teams from '@/sections/Teams';
import Groups from '@/sections/Group';

const Bracket = dynamic(() => import("@/sections/Bracket"), { ssr: false })

export default function Home() {
  return (
    <div>
      <Navbar
        fluid={true}
        rounded={true}
      >
        <Navbar.Brand href="#">
          <img
            src="/favicon.ico"
            className="mr-3 h-6 sm:h-9"
            alt="Flowbite Logo"
          />
          <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
            Golomt IT Dota 2
          </span>
        </Navbar.Brand>
        {/* <div className="flex md:order-2">
          <Button>
            Teams
          </Button>
          <Navbar.Toggle />
        </div> */}
        <Navbar.Collapse>
          <Navbar.Link href="#teams">
            Teams
          </Navbar.Link>
          <Navbar.Link href="#groups">
            Group Stage
          </Navbar.Link>
          <Navbar.Link href="#bracket">
            Bracket
          </Navbar.Link>
          <Navbar.Link
            href="#players"
            active={true}
          >
            Players
          </Navbar.Link>



        </Navbar.Collapse>
      </Navbar>
      <Teams />
      <Groups />
      <Bracket />
      <Players />
    </div>
  )
}
