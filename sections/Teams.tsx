import React from 'react';
import { Card } from 'flowbite-react'
const players: any = [
    {
        "name": "Team Oldies",
        "captain": "Ulzii-Badrakh",
    },
    {
        "name": "Team GANTU",
        "captain": "Gantulga",
    },
    {
        "name": "Team Buyanaa",
        "captain": "Buyan-Orgikh",
    },
    {
        "name": "Ultra Sonic",
        "captain": "Turbold",
    },
    {
        "name": "Fat Monkeys",
        "captain": "Altanzaviya",
    },
    {
        "name": "Team Imerald",
        "captain": "Ganbagana",
    }
]

const Teams = (props: any) => {
    return (
        <>
            <div id="teams" className='col-span-12 my-4'>
                <p className='text-center text-xl font-bold my-4'>Teams</p>
            </div>
            <div className='grid grid-cols-1 xs:grid-cols-1 sm:grid-cols-2 p-5 lg:grid-cols-3 gap-4'>

                {
                    players.map((player: any, index: number) => {
                        return <div className='w-full'>
                            <Card
                                imgAlt="Meaningful alt text for an image that is not purely decorative"
                            // imgSrc="/player.png"
                            >
                                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    {player.name}
                                </h5>

                                <div className='flex justify-between items-center'>
                                </div>
                                <p className="font-normal text-gray-700 dark:text-gray-400">
                                    Captain: {player.captain}
                                </p>
                            </Card>
                        </div>
                    })
                }
            </div>
        </>
    )
}

export default Teams;