import { Table } from 'flowbite-react';
import React from 'react';

const Groups = (props: any) => {
    return <>
        <div id="groups" className='col-span-12 my-4'>
            <p className='text-center text-xl font-bold my-4'>Group Stage</p>
        </div>
        <div className='grid grid-cols-1 xs:grid-cols-1 sm:grid-cols-1 p-5 lg:grid-cols-2 gap-4'>
            <div>
                <p className='text-center text-sm font-bold my-4'>Group A</p>
                <Table>
                    <Table.Head>
                        <Table.HeadCell>
                            Team
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Team Oldies
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Ultra Sonic
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Fat Monkey
                        </Table.HeadCell>

                    </Table.Head>
                    <Table.Body className="divide-y">
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Team Oldies
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                            <Table.Cell>
                                09:00
                            </Table.Cell>
                            <Table.Cell>
                                10:30
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Ultra Sonic
                            </Table.Cell>
                            <Table.Cell>
                                09:00
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                            <Table.Cell>
                                12:00
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Fat Monkey
                            </Table.Cell>
                            <Table.Cell>
                                10:30
                            </Table.Cell>
                            <Table.Cell>
                                12:00
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                        </Table.Row>

                    </Table.Body>
                </Table>
            </div>
            <div>
                <p className='text-center text-sm font-bold my-4'>Group A</p>
                <Table>
                    <Table.Head>
                        <Table.HeadCell>
                            Team
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Team Buyanaa
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Imerald
                        </Table.HeadCell>
                        <Table.HeadCell>
                            Team GANTU
                        </Table.HeadCell>

                    </Table.Head>
                    <Table.Body className="divide-y">
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Team Buyanaa
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                            <Table.Cell>
                                09:00
                            </Table.Cell>
                            <Table.Cell>
                                10:30
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Imerald
                            </Table.Cell>
                            <Table.Cell>
                                09:00
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                            <Table.Cell>
                                12:00
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
                            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                Team GANTU
                            </Table.Cell>
                            <Table.Cell>
                                10:30
                            </Table.Cell>
                            <Table.Cell>
                                12:00
                            </Table.Cell>
                            <Table.Cell>
                                N/A
                            </Table.Cell>
                        </Table.Row>

                    </Table.Body>
                </Table>
            </div>
        </div>

    </>
}

export default Groups;