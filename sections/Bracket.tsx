import React from 'react';
import { SingleEliminationBracket, SVGViewer, Match } from '@g-loot/react-tournament-brackets';
import { Card } from 'flowbite-react';

const matches = [
    {
        "id": 260005,
        "name": "Semi - Final LEG 1",
        "nextMatchId": 260007, // Id for the nextMatch in the bracket, if it's final match it must be null OR undefined
        "tournamentRoundText": "4", // Text for Round Header
        "startTime": "2023-05-06 13:30",
        "state": "NO_SHOW", // 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | 'DONE' | 'SCORE_DONE' Only needed to decide walkovers and if teamNames are TBD (to be decided)
        "participants": [
            {
                "id": "1", // Unique identifier of any kind
                "resultText": "", // Any string works
                "isWinner": false,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | null
                "name": "TBD"
            },
            {
                "id": "2",
                "resultText": null,
                "isWinner": true,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY'
                "name": "TBD"
            },

        ]
    },
    {
        "id": 260006,
        "name": "Semi - Final LEG 2",
        "nextMatchId": 260007, // Id for the nextMatch in the bracket, if it's final match it must be null OR undefined
        "tournamentRoundText": "3", // Text for Round Header
        "startTime": "2023-05-06 13:30",
        "state": "NO_SHOW", // 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | 'DONE' | 'SCORE_DONE' Only needed to decide walkovers and if teamNames are TBD (to be decided)
        "participants": [
            {
                "id": "3", // Unique identifier of any kind
                "resultText": "", // Any string works
                "isWinner": false,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | null
                "name": "TBD"
            },
            {
                "id": "4",
                "resultText": null,
                "isWinner": true,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY'
                "name": "TBD"
            }

        ]
    },
    {
        "id": 260007,
        "name": "Grand Final",
        "nextMatchId": null, // Id for the nextMatch in the bracket, if it's final match it must be null OR undefined
        "tournamentRoundText": "4", // Text for Round Header
        "startTime": "2023-05-06 17:00",
        "state": "NO_SHOW", // 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | 'DONE' | 'SCORE_DONE' Only needed to decide walkovers and if teamNames are TBD (to be decided)
        "participants": [
            {
                "id": "5", // Unique identifier of any kind
                "resultText": "", // Any string works
                "isWinner": false,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY' | null
                "name": "TBD"
            },
            {
                "id": "6",
                "resultText": null,
                "isWinner": true,
                "status": null, // 'PLAYED' | 'NO_SHOW' | 'WALK_OVER' | 'NO_PARTY'
                "name": "TBD"
            }

        ]
    }
]
const Bracket = (props: any) => {
    return (
        <>
            <div id="bracket" className='col-span-12 my-4'>
                <p className='text-center text-xl font-bold my-4'>Bracket</p>
            </div>

            <Card className='flex justify-center align-center m-5'>
                <SingleEliminationBracket

                    matches={matches}
                    matchComponent={Match}
                    svgWrapper={({ children, ...props }) => (
                        <SVGViewer width={700}  {...props}>
                            {children}
                        </SVGViewer>
                    )}
                />
            </Card>
        </>
    )
}

export default Bracket;