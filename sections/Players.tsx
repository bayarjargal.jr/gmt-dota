import React from 'react';
import { Card } from 'flowbite-react'
const players: any = [
    {
        "Name": "Baatar",
        "Rank": "Legend",
        "Role": "Core (1)",
        "Department": "MTG - PHH",
        "Team": "Team Buyanaa"
    },
    {
        "Name": "Enkhbold",
        "Rank": "Legend",
        "Role": "All",
        "Department": "EUG - MABH",
        "Team": "Team Buyanaa"
    },
    {
        "Name": "Khuder",
        "Rank": "Guardian",
        "Role": "Core (3), Support",
        "Department": "MTG - DBTH",
        "Team": "Team Buyanaa"
    },
    {
        "Name": "Munkhkhuleg",
        "Rank": "Unranked",
        "Role": "Support, Core (3)",
        "Department": "MTG - SHAH",
        "Team": "Team Buyanaa"
    },
    {
        "Name": "Buyan-Orgikh",
        "Rank": "Archon",
        "Role": "Core (3)",
        "Department": "DUH - DAA",
        "Team": "Team Buyanaa"
    },
    {
        "Name": "Khuslen",
        "Rank": "Immortal",
        "Role": "Core (1)",
        "Department": "DBG - TsHAH",
        "Team": "Team Imerald"
    },
    {
        "Name": "Tsogt - Ochir",
        "Rank": "Crusader",
        "Role": "Support(4), Core(3)",
        "Department": "MTG - SH",
        "Team": "Team Imerald"
    },
    {
        "Name": "Tuguldur",
        "Rank": "Herald",
        "Role": "Support",
        "Department": "EUG - MABH",
        "Team": "Team Imerald"
    },
    {
        "Name": "Tumen-Ulzii",
        "Rank": "Unranked",
        "Role": "Support",
        "Department": "MTG - SH",
        "Team": "Team Imerald"
    },
    {
        "Name": "Ganbagana",
        "Rank": "Crusader",
        "Role": "Core (3)",
        "Department": "EUG - MABH",
        "Team": "Team Imerald"
    },
    {
        "Name": "Bayarjargal",
        "Rank": "Divine",
        "Role": "Core(2)/Support(4)",
        "Department": "MTG  - PHH",
        "Team": "Team GANTU"
    },
    {
        "Name": "Bilegsaikhan",
        "Rank": "Archon",
        "Role": "Core (3)",
        "Department": "MTG - SH",
        "Team": "Team GANTU"
    },
    {
        "Name": "Sukhbat",
        "Rank": "Archon",
        "Role": "Support, Core(2, 3)",
        "Department": "MTG - DBTH",
        "Team": "Team GANTU"
    },
    {
        "Name": "Munkhnyam",
        "Rank": "Unranked",
        "Role": "Support",
        "Department": "MTG - SH",
        "Team": "Team GANTU"
    },
    {
        "Name": "Gantulga",
        "Rank": "Archon",
        "Role": "Support",
        "Department": "DBH",
        "Team": "Team GANTU"
    },
    {
        "Name": "Munkhtulga",
        "Rank": "Unranked",
        "Role": "All",
        "Department": "EX",
        "Team": "Ultra Sonic"
    },
    {
        "Name": "Munkhtushig",
        "Rank": "Legend",
        "Role": "All",
        "Department": "MTG - SH",
        "Team": "Ultra Sonic"
    },
    {
        "Name": "Tuvshinsaikhan",
        "Rank": "Guardian",
        "Role": "Core",
        "Department": "MTG - PHH",
        "Team": "Ultra Sonic"
    },
    {
        "Name": "Unubold",
        "Rank": "Unranked",
        "Role": "Support",
        "Department": "MTG - DBTH",
        "Team": "Ultra Sonic"
    },
    {
        "Name": "Turbold",
        "Rank": "Crusader",
        "Role": "Support",
        "Department": "EX",
        "Team": "Ultra Sonic"
    },
    {
        "Name": "Ankhbayar",
        "Rank": "Archon",
        "Role": "All",
        "Department": "MTG - SHAH",
        "Team": "Team Oldies"
    },
    {
        "Name": "Munkhsuld",
        "Rank": "Legend",
        "Role": "All",
        "Department": "EX",
        "Team": "Team Oldies"
    },
    {
        "Name": "Tsogjavkhlan",
        "Rank": "Archon",
        "Role": "Support",
        "Department": "MTG - PHH",
        "Team": "Team Oldies"
    },
    {
        "Name": "Uuganbayar",
        "Rank": "Guardian",
        "Role": "Support",
        "Department": "MTG - PHH",
        "Team": "Team Oldies"
    },
    {
        "Name": "Ulziibadrakh",
        "Rank": "Archon",
        "Role": "Core(3), Support(5)",
        "Department": "DUH - DAA",
        "Team": "Team Oldies"
    },
    {
        "Name": "Altangerel",
        "Rank": "Crusader",
        "Role": "Support",
        "Department": "MTG - SH",
        "Team": "Fat Monkey"
    },
    {
        "Name": "Erdenetogtokh",
        "Rank": "Immortal",
        "Role": "Core(1)",
        "Department": "DUH - DAA",
        "Team": "Fat Monkey"
    },
    {
        "Name": "Radnaabayar",
        "Rank": "Unranked",
        "Role": "Support",
        "Department": "MTG - SH",
        "Team": "Fat Monkey"
    },
    {
        "Name": "Tsogtsaikhan",
        "Rank": "Unranked",
        "Role": "Support",
        "Department": "MTG - SHAH",
        "Team": "Fat Monkey"
    },
    {
        "Name": "Altanzaviya",
        "Rank": "Crusader",
        "Role": "Core (1)",
        "Department": "EX",
        "Team": "Fat Monkey"
    }
]

const Players = (props: any) => {
    return (
        <>
            <div id="players" className='col-span-12 my-4'>
                <p className='text-center text-xl font-bold my-4'>Players</p>
            </div>
            <div className='grid grid-cols-1 xs:grid-cols-1 sm:grid-cols-2 p-5 lg:grid-cols-5 gap-4'>

                {
                    players.map((player: any, index: number) => {
                        return <div className='w-full'>
                            <Card
                                imgAlt="Meaningful alt text for an image that is not purely decorative"
                                imgSrc="/player.png"
                            >
                                <div className='flex justify-between items-center'>
                                    <div>
                                        <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                            {player.Name}
                                        </h5>
                                        <p className='text-slate-500'>
                                            {player.Team}
                                        </p>
                                    </div>
                                    <img className='w-20' src={`/ranks/${player.Rank.toLowerCase()}.png`} />
                                </div>
                                <p className="font-normal text-gray-700 dark:text-gray-400">
                                    Role: {player.Role}
                                </p>
                                <p className="font-normal text-gray-700 dark:text-gray-400">
                                    Department: {player.Department}
                                </p>
                            </Card>
                        </div>
                    })
                }
            </div>
        </>
    )
}

export default Players;